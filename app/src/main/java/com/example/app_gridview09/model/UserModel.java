package com.example.app_gridview09.model;

public class UserModel {
    String strImage;
    String strName;
    String strAddress;

    public UserModel(String strImage, String strName, String strAddress) {
        this.strImage = strImage;
        this.strName = strName;
        this.strAddress = strAddress;
    }

    public String getStrImage() {
        return strImage;
    }

    public void setStrImage(String strImage) {
        this.strImage = strImage;
    }

    public String getStrName() {
        return strName;
    }

    public void setStrName(String strName) {
        this.strName = strName;
    }

    public String getStrAddress() {
        return strAddress;
    }

    public void setStrAddress(String strAddress) {
        this.strAddress = strAddress;
    }
}
