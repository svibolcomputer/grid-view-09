package com.example.app_gridview09;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class CustomGridViewDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gv_custom_item);

        ImageView mImvPicture = findViewById(R.id.img_profile);
        TextView mTvName = findViewById(R.id.tv_name);
        TextView mTvAddress = findViewById(R.id.tv_address);

        Bundle bundle = getIntent().getExtras();
//        mImvPicture.setImageResource(bundle.getInt("kPicture"));
        Picasso.get().load(bundle.getString("kPicture")).into(mImvPicture);
        mTvName.setText(bundle.getString("kName"));
        mTvAddress.setText(bundle.getString("kAddress"));



    }
}
