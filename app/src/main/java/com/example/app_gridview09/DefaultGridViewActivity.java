package com.example.app_gridview09;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

public class DefaultGridViewActivity extends AppCompatActivity {

    String[] arrItems = new String[80];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default_adapter_with_grid_view);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            TextView mTvDisplay = findViewById(R.id.tv_display);

            mTvDisplay.setText(bundle.getString("kDisplay"));
        }

        GridView mGvItems = findViewById(R.id.gv_items);

        for(int i = 0; i < 80; i++){
            arrItems[i] = "A "+ i ;
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrItems);
//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
//                this,
//                R.layout.gv_item,
//                arrItems);
        mGvItems.setAdapter(arrayAdapter);


    }
}
