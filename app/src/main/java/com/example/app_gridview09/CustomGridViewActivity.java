package com.example.app_gridview09;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.example.app_gridview09.custom.CustomAdapterWithModel;
import com.example.app_gridview09.model.UserModel;
import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;

public class CustomGridViewActivity extends AppCompatActivity {

    private GridView mGvItem;
    private List<UserModel> userModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_grid_view);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            TextView mTvDisplay = findViewById(R.id.tv_display);

            mTvDisplay.setText(bundle.getString("kDisplay"));
        }

        mGvItem = findViewById(R.id.gv_items);

        Faker faker = new Faker();
        String strImg;
        String strName;
        String strAddress;

        for(int i = 0; i < 25; i++){
            strImg = faker.avatar().image();
            strName = faker.name().fullName();
            strAddress = faker.address().fullAddress();
            userModelList.add(new UserModel(strImg, strName, strAddress));

        }

        CustomAdapterWithModel customAdapterWithModel = new CustomAdapterWithModel(this, this.userModelList);
        mGvItem.setAdapter(customAdapterWithModel);

        mGvItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserModel userModel = (UserModel) parent.getItemAtPosition(position);
                Intent intent = new Intent(CustomGridViewActivity.this, CustomGridViewDetailActivity.class);
                intent.putExtra("kPicture",userModel.getStrImage());
                intent.putExtra("kName",userModel.getStrName());
                intent.putExtra("kAddress",userModel.getStrAddress());
                startActivity(intent);
            }
        });





    }
}
