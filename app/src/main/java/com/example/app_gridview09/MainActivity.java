package com.example.app_gridview09;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.mifmif.common.regex.Main;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button mBtnDefaultAdapterWithGridView = findViewById(R.id.btn_default_grid_view);
        mBtnDefaultAdapterWithGridView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DefaultGridViewActivity.class);
                intent.putExtra("kDisplay","Default adapter with grid view");
                startActivity(intent);
            }
        });

        Button mBtnCustomGridView = findViewById(R.id.btn_custom_grid_view);
        mBtnCustomGridView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CustomGridViewActivity.class);
                intent.putExtra("kDisplay","Custom grid view");
                startActivity(intent);
            }
        });

        Button mBtnHowWork = findViewById(R.id.btn_home_work);
        mBtnHowWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HomeWorkActivity.class);
                startActivity(intent);
            }
        });

    }
}
