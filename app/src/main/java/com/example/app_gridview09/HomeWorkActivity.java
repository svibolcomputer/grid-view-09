package com.example.app_gridview09;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.example.app_gridview09.custom.CustomAdapterWithModel;
import com.example.app_gridview09.custom.CustomAdapterWithModel_HW;
import com.example.app_gridview09.model.UserModel;
import com.example.app_gridview09.model.UserModel_HW;
import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;

public class HomeWorkActivity extends AppCompatActivity {

    private GridView mGvItemHW;
    private List<UserModel_HW> userModel_hwList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work);

        mGvItemHW = findViewById(R.id.gv_items_hw);

        Faker faker = new Faker();
        String strImg;
        String strName;
        String strAddress;

        for(int i = 0; i < 12; i++){
            strImg = faker.avatar().image();
            strName = faker.name().fullName();
            strAddress = faker.address().fullAddress();
            userModel_hwList.add(new UserModel_HW(strImg, strName, strAddress));

        }

        CustomAdapterWithModel_HW customAdapterWithModel_hw = new CustomAdapterWithModel_HW(this, this.userModel_hwList);
        mGvItemHW.setAdapter(customAdapterWithModel_hw);

//        Toast.makeText(HomeWorkActivity.this, "mmmmmmmmm", Toast.LENGTH_SHORT).show();


        mGvItemHW.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(HomeWorkActivity.this, ""+ position, Toast.LENGTH_SHORT).show();
            }
        });

//        mGvItemHW.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                Toast.makeText(HomeWorkActivity.this, "testfgfg", Toast.LENGTH_SHORT).show();
////                UserModel_HW userModel_hw = (UserModel_HW) parent.getItemAtPosition(position);
////                Intent intent = new Intent(HomeWorkActivity.this, HomeWorkDetailActivity.class);
////                intent.putExtra("kPicture",userModel_hw.getStrImage());
////                intent.putExtra("kName",userModel_hw.getStrName());
////                intent.putExtra("kAddress",userModel_hw.getStrAddress());
////                startActivity(intent);
//            }
//        });
    }
}
