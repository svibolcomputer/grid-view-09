package com.example.app_gridview09.custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app_gridview09.R;
//import com.example.app_gridview09.model.UserModel;
import com.example.app_gridview09.model.UserModel_HW;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CustomAdapterWithModel_HW extends BaseAdapter {

    private ImageView mImvProfile;
    private TextView mTvName;

    private Context mContext;
    private List<UserModel_HW> objUser = new ArrayList<>();

    public CustomAdapterWithModel_HW(Context mContext, List<UserModel_HW> objUser) {
        this.mContext = mContext;
        this.objUser = objUser;
    }

    @Override
    public int getCount() {
        return objUser.size();
    }

    @Override
    public Object getItem(int position) {
        return objUser.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null ){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.gv_custom_item_hw, null);
        }

        initializeWidgets(convertView);

        initializeWidgetValues(objUser.get(position));

        return convertView;
    }


    private void initializeWidgets(View view){
//        mImvProfile = view.findViewById(R.id.img_profile_hw);
        mImvProfile = view.findViewById(R.id.ci_profile);
        mTvName = view.findViewById(R.id.tv_Name_hw);

    }

    private void initializeWidgetValues(UserModel_HW userModel_hw) {
        Picasso.get().load(userModel_hw.getStrImage()).into(this.mImvProfile);
        mTvName.setText(userModel_hw.getStrName());
    }

}
