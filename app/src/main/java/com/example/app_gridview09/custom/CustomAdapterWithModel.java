package com.example.app_gridview09.custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app_gridview09.R;
import com.example.app_gridview09.model.UserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CustomAdapterWithModel extends BaseAdapter {

    private ImageView mImvProfile;
    private TextView mTvName;
    private TextView mTvAddress;

    private Context mContext;
    private List<UserModel> objUser = new ArrayList<>();

    public CustomAdapterWithModel(Context mContext, List<UserModel> objUser) {
        this.mContext = mContext;
        this.objUser = objUser;
    }

    @Override
    public int getCount() {
        return objUser.size();
    }

    @Override
    public Object getItem(int position) {
        return objUser.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null ){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.gv_custom_item, null);
        }

        initializeWidgets(convertView);

        initializeWidgetValues(objUser.get(position));

        return convertView;
    }

    private void initializeWidgets(View view){
        mImvProfile = view.findViewById(R.id.img_profile);
        mTvName = view.findViewById(R.id.tv_name);
        mTvAddress = view.findViewById(R.id.tv_address);
    }

    private void initializeWidgetValues(UserModel userModel){
        //        mImvProfile.setImageResource(objUser.get(position).getStrImage());
//        mImvProfile.setImageResource(R.drawable.ic_launcher_foreground);
        Picasso.get().load(userModel.getStrImage()).into(this.mImvProfile);
        mTvName.setText(userModel.getStrName());
        mTvAddress.setText(userModel.getStrAddress());
    }
}
